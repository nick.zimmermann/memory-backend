const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const ScoresRoute = require('./routes/ScoresRoute');
require('dotenv').config();
const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors({ origin: 'https://memory.nickzimmermann.com' }));

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
  console.log('Connecting to MongoDB');
});

app.use('/scores', ScoresRoute);

app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`));
