const express = require('express');
const Scores = require('../models/scores');

const router = express.Router();

router.get('/', async (req, res) => {
  const scores = await Scores.find();
  res.json(scores);
});

router.get('/:username', async (req, res) => {
  const scores = await Scores.find({ username: req.params.username });
  res.json(scores);
});

router.post('/', async (req, res) => {
  const scores = await new Scores(req.body);
  await scores.save();
  res.json(scores);
});

router.delete('/:id', async (req, res) => {
  const scores = await Scores.findByIdAndDelete(req.params.id);
  res.json(scores);
});

module.exports = router;
