const mongoose = require('mongoose');

const scoresSchema = new mongoose.Schema(
  {
    username: String,
    score: Number,
  },
  { timestamps: true }
);

const Scores = mongoose.model('Scores', scoresSchema);

module.exports = Scores;
